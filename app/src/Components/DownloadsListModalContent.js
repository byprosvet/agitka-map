import React from 'react';
import { Alert } from 'antd';

import { FilePdfOutlined } from '@ant-design/icons';
import './DownloadsListModalContent.css';

const DownloadsListModalContent = (props) => {
    return (
        <div className="content">
            <h4>Дорогие друзья!</h4>
            <p>Здесь вы можете найти информационные материалы для распространения. Делитесь!</p>

            {!props.docs?.length ?
                <Alert
                    className="mb-2"
                    type="info"
                    message="На данный момент нет доступных материалов. Пожалуйста, проверьте позже."
                    showIcon /> : ''
            }

            {props.docs?.length ?
                <ul className="downloads-list">
                    {props.docs.map(doc =>
                        <li className="downloads-list-item" key={doc.id}>
                            <FilePdfOutlined
                                className="mr-2"
                                style={{fontSize: '1.75rem'}}/
                            >
                                <a
                                    href={doc.file_link}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    {doc.title}
                                </a>
                        </li>
                    )}
                </ul> : ''
            }

            <p className="mb-0">Большое спасибо!</p>
        </div>
    );
}

export default DownloadsListModalContent;
