import React, { Component } from 'react';
import { YMaps, Map, ObjectManager } from 'react-yandex-maps';
import './AgitationMap.css';
import { getPointFeature } from './PointFeature';
import { createAgitationPlacemark } from './AgitationPlacemark';
import { CONFIG } from '../config';
import { arrayToMap } from '../Core/MapUtils';

class AgitationMap extends Component {
    state = {
        agitationPlacemark: null,
    };

    removeAgitationPlacemark() {
        this.map.geoObjects.remove(this.state.agitationPlacemark);
        this.setState({ agitationPlacemark: null });
    }

    onLoad = (ymaps) => {
        this.ymaps = ymaps;
    }

    onMapReady = (map) => {
        if (this.map) return;

        this.map = map;
        this.map.events.add('click', this.onMapClick);

        document.addEventListener('click', (event) => {
            if (event.target.dataset.balloonAction === 'add') {
                this.onAddPointClick();
            }

            if (event.target.dataset.balloonAction === 'cancel') {
                this.onCancelPointClick();
            }

            if (event.target.dataset.balloonAction === 'add-more') {
                const index = event.target.dataset.pointIndex;
                this.onAddAnotherPointClick({ index });
            }
        });
    }

    /**
     * @see https://tech.yandex.ru/maps/jsbox/2.1/event_reverse_geocode
     */
    onMapClick = (event) => {
        const coordinates = event.get('coords');

        if (this.state.agitationPlacemark) {
            this.state.agitationPlacemark.geometry.setCoordinates(coordinates);
        } else {
            const agitationPlacemark = createAgitationPlacemark(this.ymaps, coordinates);
            this.map.geoObjects.add(agitationPlacemark);

            this.setState({ agitationPlacemark });
        }

        this.state.agitationPlacemark.balloon.open();
    }

    onAddPointClick = () => {
        const [lat, lon] = this.state.agitationPlacemark.geometry.getCoordinates();
        this.state.agitationPlacemark.balloon.close();

        this.props.onAddPoint({ lat, lon });
    }

    onAddAnotherPointClick = ({ index }) => {
        this.removeAgitationPlacemark();

        const { lat, lon } = this.props.points[index];

        this.props.onAddPoint({ lat, lon });
        // window.ym(65931997,'reachGoal','agitation');
    }

    onCancelPointClick = () => {
        this.removeAgitationPlacemark();
    }

    render() {
        const docIdMap = arrayToMap(this.props.docs, 'id');

        return (
            <div className="map-wrapper">
                <YMaps query={{ apikey: CONFIG.yandexMapsApiKey }}>
                    <Map
                        className="map"
                        defaultState={{
                            center: [53.906689, 27.495482],
                            zoom: 7,
                            controls: [
                                'geolocationControl',
                                'fullscreenControl',
                                'searchControl',
                                'zoomControl',
                            ],
                        }}
                        defaultOptions={{
                            maxZoom: 18
                        }}
                        modules={[
                            'control.GeolocationControl',
                            'control.FullscreenControl',
                            'control.SearchControl',
                            'control.ZoomControl',
                        ]}
                        onLoad={this.onLoad}
                        instanceRef={this.onMapReady}
                    >
                        <ObjectManager
                            defaultOptions={{
                                clusterize: true,
                                gridSize: 64,
                                clusterOpenBalloonOnClick: true,
                                clusterDisableClickZoom: false,
                                clusterHideIconOnBalloonOpen: false,
                                geoObjectHideIconOnBalloonOpen: false,
                            }}
                            defaultClusters={{
                                clusterIconLayout: 'default#pieChart',
                                clusterIconPieChartRadius: 30,
                                clusterIconPieChartCoreRadius: 20,
                                clusterIconPieChartStrokeWidth: 5,
                            }}
                            modules={[
                                'geoObject.addon.balloon',
                                'geoObject.addon.hint',
                                'objectManager.addon.clustersBalloon',
                                'objectManager.addon.objectsBalloon',
                                'objectManager.addon.objectsHint',
                                'Placemark',
                                'layout.PieChart',
                            ]}
                            features={this.props.points.map((point, index) => getPointFeature({
                                doc: docIdMap.get(point.document_id),
                                point,
                                index,
                            }))}
                        ></ObjectManager>
                    </Map>
                </YMaps>
            </div>
        );
    }
}

export default AgitationMap;
