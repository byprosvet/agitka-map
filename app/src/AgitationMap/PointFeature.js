import { escapeHtml } from '../Core/Security';


const DEFAULT_COLOR = '#8c8c8c';


function getPointBalloonContent({point, index, doc}) {
    const docTitle = doc?.title ? escapeHtml(doc.title) : 'Больше не доступны';
    const porch = point.porch ? escapeHtml(point.porch) : '--';
    const coordinates = escapeHtml(`${point.lat}, ${point.lon}`);

    const footerHtml = `
        <div>
            <button
                class="ant-btn ant-btn-link p-0"
                style="padding: 0"
                data-point-index="${index}"
                data-balloon-action="add-more"
            >
                Добавить еще
            </button>
        </div>`

    return `
        <div data-coordinates="${coordinates}">
            <div class="mb-1">
                <div>
                    <b>Материалы:</b> ${docTitle}
                </div>
                <div>
                    <b>Подъезд:</b> ${porch}
                </div>
            </div>
            ${footerHtml}
        </div>`
}

function getClusterCaption({point, index, doc}) {
    const porch = point.porch ? `Подъезд ${point.porch}` : `Точка ${index}`;
    const docTitle = doc?.title ?? 'НЕ ДОСТУПНО';
    
    return `${docTitle} (${porch})`;
}

export function getPointFeature({point, index, doc}) {
    // Generate a dynamic Id to make a map rerendered properly.
    const now = Date.now();
    const id = `${index}_${now}`;

    return {
        type: 'Feature',
        id,
        geometry: {
            type: 'Point',
            coordinates: [point.lat, point.lon]
        },
        options: {
            preset: 'islands#circleDotIcon',
            iconColor: doc?.color ?? DEFAULT_COLOR,
        },
        properties: {
            balloonContent: getPointBalloonContent({point, index, doc}),
            hintContent: doc?.title ?? 'Материалы больше не доступны',
            clusterCaption: getClusterCaption({point, index, doc}),
        }
    };
}
