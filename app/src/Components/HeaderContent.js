import React from 'react';
import './HeaderContent.less';
import { Button } from 'antd';

import { MenuOutlined } from '@ant-design/icons';

const HeaderContent = (props) => {
    return (
        <div className="header-content">
            <div className="brand">
                {/* <div className="brand-logo"></div> */}
                <h2 className="brand-name">Карта информирования</h2>
            </div>
            {props.hasMenu &&
                <Button
                    type=""
                    className="gamburgerMenu"
                    shape="round"
                    icon={<MenuOutlined />}
                    onClick={props.onShowMenu}
                >
                    <span className="s">Меню</span>
                </Button>
            }
        </div>
    );
}

export default HeaderContent;
