const uuid = require("uuid");
const config = require("../config");
const client = require("prom-client");
const metric = new client.Gauge({ name: "points_total", help: "points_total" });
const logger = require("../logger");

const ApprovalStatus = {
  IN_REVIEW: "0",
  APPROVED: "1",
  REJECTED: "2",
};

class Points {
  constructor(sheets, drive) {
    this.sheets = sheets;
    this.drive = drive;
    this.points = [];
  }

  async fetch() {
    const formData = await this.sheets.spreadsheets.values.get({
      spreadsheetId: config.points.tableId,
      range: config.points.page + "!B2:J",
    });

    return formData.data.values
      .filter((row) => row[4].trim() !== ApprovalStatus.REJECTED)
      .map(function (row) {
        const point = row[0].split(",");

        return {
          lat: point[0],
          lon: point[1],
          porch: row[2],
          approvalStatus: row[4].trim(),
          mailbox: !!(row[7] && row[7].trim() === "1"),
          document_id: row[8] && row[8].trim() ? row[8].trim() : "0",
        };
      });
  }

  async update() {
    this.points = await this.fetch().catch((error) => {
      logger.error("Points update error", { error });
      return this.points;
    });
    metric.set(this.points.length);
  }

  async append(data) {
    const row = await this.sheets.spreadsheets.values.append({
      spreadsheetId: config.points.tableId,
      range: config.points.page,
      valueInputOption: "USER_ENTERED",
      insertDataOption: "INSERT_ROWS",
      resource: {
        values: [
          [
            new Date().toISOString(),
            data.lat + "," + data.lon,
            "",
            data.porch,
            "",
            data.approvalStatus,
            data.name,
            "",
            data.mailbox,
            data.document_id,
          ],
        ],
      },
    });

    this.points.push({
      lat: data.lat,
      lon: data.lon,
      porch: data.porch,
      approvalStatus: data.approvalStatus,
      mailbox: !!data.mailbox,
      document_id: data.document_id,
    });
  }
}

module.exports.Points = Points;
module.exports.ApprovalStatus = ApprovalStatus;
