# Agitation Map

Координация информирования. Данное приложение создано для волонтеров, чтобы
координировать распространение листовок и иной печатной информации.

В качестве базы данных используются таблицы Google(Google Sheets). Используется
карта Yandex Maps.

![Application example](./docs/example.png)

### Requirements/Требования

[![pipeline status](https://gitlab.com/byprosvet/agitka-map/badges/master/pipeline.svg)](https://gitlab.com/byprosvet/agitka-map/-/commits/master)

1. Выполнить шаг 1 из инструкции
   https://developers.google.com/sheets/api/quickstart/nodejs .
   и затем положить файл credentials.json в папку `/api`
1. Запустить файл `generate_token.js`,
   перейти по ссылке,
   дать доступ на чтение и запись
   и ввести сгенерированный ключ в консоль
1. Взять ссылку из ошибки - перейти в консоли и разрешить использование гугл-апи.
1. Перенести соддержимое вайлов `credentials.json` и `token.json` в `.env`.
1. В `.env` указать таблицу и страницу с пользователями в поле `user`.
1. Создать Google-таблицу для точек и документов
   (пример: https://docs.google.com/spreadsheets/d/1eZVIszpeulXPcjKmbKPsIzRhAfCZSgnGIv8L8eSJcpE/edit?usp=sharing)
1. Созддать отельную папку для загрруженных пользователями фотографий на гугл-диске.
1. В `.env` указать ид созданной папки.

### Installation and run

- `yarn build`
- `docker-compose up agitka`

### API & Routes

Информация про использование api и доступных роутов [можно найти здесь](./docs/API_ROUTES.md)
