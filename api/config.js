module.exports = {
  port: Number(process.env.PORT),
  cacheLife: Number(process.env.CACHE_LIFE),
  gAuth: {
    credential: JSON.parse(process.env.GOOGLE_CREDENTIALS),
    token: JSON.parse(process.env.GOOGLE_TOKEN),
  },
  users: {
    tableId: process.env.USERS_TABLE,
    page: process.env.USERS_PAGE,
  },
  points: {
    tableId: process.env.POINTS_TABLE,
    page: process.env.POINTS_PAGE,
  },
  documents: {
    tableId: process.env.DOCUMENTS_TABLE,
    page: process.env.DOCUMENTS_PAGE,
  },
  driveFolderId: process.env.DRIVE_FOLDER,
  botName: "TgBot",
  active: true,
};
