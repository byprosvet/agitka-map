export function arrayToMap(data, key) {
    const pairs = data.map(item => [item[key], item]); 

    return new Map(pairs);
}
