const { Points } = require("./points");
const config = require("../config");
const { getSheets } = require("../google-docs");

let points;
const createPoints = async (sheets, drive) => {
  points = new Points(sheets, drive);
  await points.update();
  setInterval(points.update.bind(points), config.cacheLife);
};

module.exports = async (req, res) => {
  const { sheets, drive } = await getSheets();
  if (!points) {
    await createPoints(sheets, drive);
  }
  return res.status(200).json(points.points);
};
