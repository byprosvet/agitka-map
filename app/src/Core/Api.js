import axios from 'axios';
import { CONFIG } from '../config';

const api = axios.create({
  baseURL: CONFIG.apiBase,
  responseType: 'json'
});

export function getAuthUrl() {
  return `${CONFIG.apiBase}/auth`;
}

export async function getRole() {
  const response = await api.get('api/role');

  return response.data.role;
}

export async function getPoints() {
  const response = await api.get('api/points');

  return response.data;
}

export async function getDocuments() {
  const response = await api.get('api/documents');

  return response.data;
}

export async function addPoint(point) {
  await api.post('api/point', point);
}
