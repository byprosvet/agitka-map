const config = require("../config");
const client = require("prom-client");
const metric = new client.Gauge({
  name: "documents_total",
  help: "documents_total",
});
const logger = require("../logger");

const STATUS_INACTIVE = "0";
const STATUS_ACTIVE = "1";

class Documents {
  constructor(sheets) {
    this.sheets = sheets;
    this.documents = [];
  }

  async fetch() {
    const formData = await this.sheets.spreadsheets.values.get({
      spreadsheetId: config.documents.tableId,
      range: config.documents.page + "!A2:F",
    });

    return (formData.data.values || [])
      .map(function (row, id) {
        if (row[3] !== STATUS_ACTIVE) {
          return null;
        }

        return {
          id: (id + 2).toString(), //bcs we start from second row
          title: row[0],
          file_link: row[1],
          color: row[2],
          order: row[5],
        };
      })
      .filter((v) => !!v)
      .sort(function (a, b) {
        return a.order - b.order;
      });
  }

  async update() {
    this.documents = await this.fetch().catch((error) => {
      logger.error("Documents update error", { error });
      return this.documents;
    });
    metric.set(this.documents.length);
  }
}

module.exports = Documents;
