import React from "react";
import ulti from "./ultimatum.jpg";

import "./UltimatumModal.css";

const UltimatumModalContent = () => {
  return (
    <div className="content">
      <p>
        <img className={"image"} src={ulti} alt="ultimatum" />
      </p>

      <h4>1 Требуем Лукашенко заявить о своем уходе</h4>
      <h4>2 Требуем остановить насилие полностью</h4>
      <h4>3 Требуем освободить всех политзаключенных</h4>
      <p>
        Если до 25 октября наши требования не будут выполнены, вся страна мирно
        выйдет на улицы с Народным Ультиматумом. И 26 октября начнётся
        национальная забастовка всех предприятий, блокировка всех дорог, обвал
        продаж в государственных магазинах.{" "}
        <span className={"bolder"}>Мы говорим хватит!</span>
      </p>
    </div>
  );
};

export default UltimatumModalContent;
