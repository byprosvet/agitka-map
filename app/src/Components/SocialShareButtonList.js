import React from 'react';
import {
    FacebookIcon,
    FacebookShareButton,
    OKIcon,
    OKShareButton,
    TwitterIcon,
    TwitterShareButton,
    TelegramIcon,
    TelegramShareButton,
    ViberIcon,
    ViberShareButton,
    VKIcon,
    VKShareButton,
} from "react-share";
import "./SocialShareButtonList.css";
import BRAND from '../brand';


/**
 * A list of "Share" buttons for social networks.
 * @see https://github.com/nygardk/react-share 
 */
const SocialShareButtonList = () => {
    const messageWithHashTags = `${BRAND.social.message} ${BRAND.social.hashTags.map(tag => `#${tag}`).join(' ')}`;

    return (
        <ul className="social-share-button-list">
            <li title="Поделиться в Facebook">
                <FacebookShareButton
                    className="social-share-button"
                    url={BRAND.social.shareUrl}
                    quote={BRAND.social.message}
                    hashtag={'#' + BRAND.social.hashTags[0]}
                >
                    <FacebookIcon size={32} round />
                </FacebookShareButton>
            </li>
            <li title="Поделиться в Twitter">
                <TwitterShareButton
                    className="social-share-button"
                    url={BRAND.social.shareUrl}
                    title={BRAND.social.message}
                    hashtags={BRAND.social.hashTags}
                >
                    <TwitterIcon size={32} round />
                </TwitterShareButton>
            </li>
            <li title="Поделиться в ВКонтакте">
                <VKShareButton
                    className="social-share-button"
                    url={BRAND.social.shareUrl}
                    title={messageWithHashTags}
                >
                    <VKIcon size={32} round />
                </VKShareButton>
            </li>
            <li title="Поделиться в Одноклассники">
                <OKShareButton
                    className="social-share-button"
                    url={BRAND.social.shareUrl}
                    title={messageWithHashTags}
                >
                    <OKIcon size={32} round />
                </OKShareButton>
            </li>
            <li title="Поделиться в Telegram">
                <TelegramShareButton
                    className="social-share-button"
                    url={BRAND.social.shareUrl}
                    title={messageWithHashTags}
                >
                    <TelegramIcon size={32} round />
                </TelegramShareButton>
            </li>
            <li title="Поделиться в Viber">
                <ViberShareButton
                    className="social-share-button"
                    url={BRAND.social.shareUrl}
                    title={BRAND.social.message}
                >
                    <ViberIcon size={32} round />
                </ViberShareButton>
            </li>
        </ul>
    );
}

export default SocialShareButtonList;
