export function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

export function isImage(file) {
  return file.type === 'image/jpeg' || file.type === 'image/png';
}

export function hasSizeWithin(file, mbs) {
  return file.size / 1024 / 1024 < mbs;
}

export function isNormalPhoto(file) {
  return isImage(file) && hasSizeWithin(file, 15);
}