import React from 'react';

const ActionLink = (props) => {
    function handleClick(e) {
        if(props.preventDefault){
            e.preventDefault();
        }
        props.handleClick();
    }
    return (
        <a href={props.link} target={props.target} onClick={handleClick}>{props.children}</a>
    );
}

export default ActionLink;