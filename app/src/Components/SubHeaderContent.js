import React from 'react';
import { Select } from 'antd';
import './SubHeaderContent.css';


const SubHeaderContent = (props) => {
    function onDocChange(docIds) {
        props.onDocChange(docIds);
    }

    const maxTagCount = Math.round(window.document.body.clientWidth / 300);

    return (
        <div className="sub-header">
            <Select
                className="doc-select"
                mode="multiple"
                value={props.selectedDocIds}
                bordered={false}
                notFoundContent="Нет данных"
                placeholder="Все материалы"
                showArrow={!props.selectedDocIds.length}
                allowClear={true}
                onChange={onDocChange}
                maxTagCount={maxTagCount}
            >
                {props.docs?.map(doc =>
                    <Select.Option
                        key={doc.id}
                        value={doc.id}
                    >
                        <span
                            className="doc-option-color mr-1"
                            style={{background: doc.color}}
                        ></span>
                        {doc.title}
                    </Select.Option>
                )}
            </Select>
        </div>
    );
}

export default SubHeaderContent;
