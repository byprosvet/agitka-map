import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Button, Drawer, Modal, Layout } from "antd";
import "antd/dist/antd.css";
import AgitationMap from "./AgitationMap/AgitationMap";
import AgitationForm from "./Components/AgitationForm";
import DownloadsListModalContent from "./Components/DownloadsListModalContent";
import HelpModalContent from "./Components/HelpModalContent";
import NavMenu from "./Components/NavMenu";
import HeaderContent from "./Components/HeaderContent";
import SubHeaderContent from "./Components/SubHeaderContent";
import { getPoints, getAuthUrl, getDocuments } from "./Core/Api";
import { arrayToMap } from "./Core/MapUtils";
import { filterPoints } from "./Core/Point";
import BRAND from "./brand";
import "./App.less";

const { Header, Content, Footer } = Layout;

class App extends Component {
  constructor() {
    super();
    this.mapRef = React.createRef();
    this.state = {
      docs: [],
      selectedDocIds: [],
      points: [],
      filteredPoints: [],
      newPoint: null,
      activeModal: null,
      showMenu: false,
    };
  }

  async componentDidMount() {
    const selectedDocIds = this.getSelectedDocIds();
    this.setState({ selectedDocIds });

    await this.loadDocuments();
    await this.loadPoints();
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.search !== prevProps.location.search) {
      const selectedDocIds = this.getSelectedDocIds();
      const filteredPoints = filterPoints(this.state.points, {
        docIds: selectedDocIds,
      });

      this.setState({ filteredPoints, selectedDocIds });
    }
  }

  async loadDocuments() {
    const docs = await getDocuments();

    this.setState({ docs });
  }

  async loadPoints() {
    const points = await getPoints();
    const filteredPoints = filterPoints(points, {
      docIds: this.state.selectedDocIds,
    });

    this.setState({ points, filteredPoints });
  }

  getSelectedDocIds() {
    const query = new URLSearchParams(this.props.location.search);
    const docIds = query.get("docIds")?.split(",") ?? [];

    const validDocIdMap = arrayToMap(this.state.docs, "id");

    return docIds.filter((id) => validDocIdMap.has(id));
  }

  onDocChange = (docIds) => {
    const params = new URLSearchParams(this.props.location.search);

    if (docIds?.length) {
      params.set("docIds", docIds);
    } else {
      params.delete("docIds");
    }

    this.props.history.push({
      pathname: "/",
      search: `?${params}`,
    });
  };

  onAddPoint = (newPoint) => {
    this.setState({ newPoint });
  };

  onPointAdded = () => {
    this.loadPoints();
    this.setState({ newPoint: null });
    this.mapRef.current.removeAgitationPlacemark();
    // window.ym(65931997, 'reachGoal', 'agitation');
  };

  onCancelAddingPoint = () => {
    this.setState({ newPoint: null });
  };

  onHelpButtonClick = () => {
    // window.ym(65931997, 'reachGoal', 'help');
    this.setState({ activeModal: "help" });
  };

  onDownloadsListButtonClick = () => {
    // window.ym(65931997, 'reachGoal', 'downloads');
    this.setState({ activeModal: "downloads" });
  };

  onUltimantumButtonClick = () => {
    // window.ym(65931997, 'reachGoal', 'downloads');
    this.setState({ activeModal: "ultimatum" });
  };

  onModalClose = () => {
    this.setState({ activeModal: null });
  };

  onShowMenu = () => {
    this.setState({ showMenu: true });
  };

  onHideMenu = (e) => {
    this.setState({ showMenu: false });
  };

  render() {
    return (
      <Layout className={"layout " + BRAND.name}>
        <Header className="header" theme="light">
          <HeaderContent
            signInUrl={getAuthUrl()}
            signInEnabled={false}
            hasMenu={true}
            onShowMenu={this.onShowMenu}
          />
          <SubHeaderContent
            docs={this.state.docs}
            selectedDocIds={this.state.selectedDocIds}
            onDocChange={this.onDocChange}
          />
        </Header>
        <Content className="content">
          <div className="agitation-map">
            <AgitationMap
              ref={this.mapRef}
              docs={this.state.docs}
              points={this.state.filteredPoints}
              onAddPoint={this.onAddPoint}
            />
          </div>

          <Drawer
            title="Отчет об информировании"
            placement="right"
            closable={true}
            visible={!!this.state.newPoint}
            width={320}
            onClose={this.onCancelAddingPoint}
          >
            {this.state.newPoint && (
              <AgitationForm
                isAdmin={false}
                initialValues={this.state.newPoint}
                docs={this.state.docs}
                defaultDocId={this.state.selectedDocIds[0]}
                onPointAdded={this.onPointAdded}
                onCancel={this.onCancelAddingPoint}
              />
            )}
          </Drawer>
          <Drawer
            placement="right"
            title="Меню"
            closable={true}
            onClose={this.onHideMenu}
            visible={this.state.showMenu}
            className="NavMenu"
          >
            <NavMenu onHideMenu={this.onHideMenu} />
          </Drawer>
        </Content>

        <Footer className="footer">
          <Button
            className="mr-2"
            type="primary"
            shape="round"
            size="large"
            placement="topCenter"
            onClick={this.onHelpButtonClick}
          >
            Старт
          </Button>
          <Button
            className="mr-2"
            type="primary"
            shape="round"
            size="large"
            placement="topCenter"
            onClick={this.onDownloadsListButtonClick}
          >
            Материалы
          </Button>
        </Footer>

        <Modal
          title="Как я могу помочь?"
          visible={this.state.activeModal === "help"}
          onCancel={this.onModalClose}
          footer={[
            <Button
              type="primary"
              key="submit"
              shape="round"
              onClick={this.onModalClose}
            >
              Понятно!
            </Button>,
          ]}
        >
          <HelpModalContent />
        </Modal>

        <Modal
          title="Материалы для печати"
          visible={this.state.activeModal === "downloads"}
          onCancel={this.onModalClose}
          footer={[
            <Button
              type="primary"
              key="submit"
              shape="round"
              onClick={this.onModalClose}
            >
              Закрыть
            </Button>,
          ]}
        >
          <DownloadsListModalContent docs={this.state.docs} />
        </Modal>
      </Layout>
    );
  }
}

// Create a new component that is "connected" to the router.
export default withRouter(App);
