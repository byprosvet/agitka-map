import React from 'react';
import { Alert } from 'antd';
import './DownloadsListModalContent.css';

const ThankYouModalContent = () => {
    return (
        <div className="content">
            <Alert
                className="mb-2"
                type="warning"
                message="Период предвыборной агитации окончен, и распространение материалов больше не разрешается."
                showIcon />

            <h4>Дорогие друзья!</h4>

            <p>Период предвыборной агитации подошел к концу.</p>

            <p>
                За этот короткий период времени, Вы смогли проинформировать огромное количество людей о том, как вместе мы 
                можем сделать выборы честными и справедливыми.
            </p>

            <p className="mb-0">Огромное Вам спасибо!</p>
        </div>
    );
}

export default ThankYouModalContent;
