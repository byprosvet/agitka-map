const winston = require("winston");
const { format, transports } = winston;

module.exports = winston.createLogger({
  format: format.combine(
    format.timestamp({
      format: "YYYY-MM-DD HH:mm:ss",
    }),
    format.errors({ stack: true }),
    format.json()
  ),
  transports: [new transports.Console()],
});
