export function timeTill(date) {
    const result = date.getTime() - Date.now();

    return result > 0 ? result : 0;
}

export function getToday() {
    const today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);

    return today;
}

export function formatTime(time, locale) {
    const today = getToday();
    const result = new Date(today.getTime() + time);

    return result.toLocaleTimeString(locale);
}
