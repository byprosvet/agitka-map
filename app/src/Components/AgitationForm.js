import React, { Component } from 'react';
import { Alert, Button, Form, Input, message, Select } from 'antd';
import { addPoint } from '../Core/Api';
import { getBase64, isNormalPhoto } from '../Core/Files';

const validateMessages = {
    required: 'Обязательное поле!',
};

/* const normalizeFile = e => {
    if (Array.isArray(e)) {
        return e;
    }

    return e && e.fileList;
}; */

class AgitationForm extends Component {
    state = {
        fileList: [],
        loading: false,
    }

    onBeforePhotoUpload = (file) => {
        this.setState({ fileList: [file] });

        return false;
    }

    onFinish = async values => {
        let photo = '';

        if (this.state.fileList.length) {
            const file = this.state.fileList[0];
            const isValidPhoto = isNormalPhoto(file);

            if (!isValidPhoto) {
                message.error('Вы можете загружать только JPG и PNG до 5 Мб');
                return;
            }
    
            photo = await getBase64(file);
        }

        try {
            this.setState({ loading: true });

            await addPoint({
                ...this.props.initialValues,
                ...values,
                photo,
            });

            message.success('Ваша точка отобразится на карте в ближайшее время.');

            this.props.onPointAdded();
        } catch (e) {
            message.error('Нам не удалось добавить точку на карту. Пожалуйста, повторите позже.');
        }

        this.setState({ loading: false });
    }

    onCancel = () => {
        this.props.onCancel();
    }

    render() {
        return (
            <div>
                <div className="mb-2">
                    <Alert
                        message="Важно!"
                        description={
                            <span>Пожалуйста, <strong>не распространяйте</strong> материалы в местах, не предназначенных для этого.</span>
                        }
                        type="warning" />
                </div>

                <Form 
                    name="nest-messages"
                    layout="vertical"
                    initialValues={{
                        porch: '',
                        document_id: this.props.defaultDocId ?? this.props.docs?.[0]?.id,
                    }}
                    validateMessages={validateMessages}
                    onFinish={this.onFinish}
                >
                    <Form.Item
                        name={['porch']}
                        label="Подъезд (для дома):"
                    >
                        <Input disabled={this.state.loading}/>
                    </Form.Item>

                    <Form.Item
                        name={['document_id']}
                        label="Материалы:"
                    >
                        <Select
                            notFoundContent="Нет данных"
                        >
                            {this.props.docs?.map(doc =>
                                <Select.Option
                                    key={doc.id}
                                    value={doc.id}
                                >
                                    {doc.title}
                                </Select.Option>
                            )}
                        </Select>
                    </Form.Item>

                    {/* 
                    {!this.props.isAdmin &&
                        <div>
                            <Form.Item
                                name="photo"
                                label="Загрузить фотографию:"
                                extra="Загрузить фотографию для подтверждения распространения материалов."
                                valuePropName="fileList"
                                getValueFromEvent={normalizeFile}
                                rules={[
                                    {required: true}
                                ]}
                            >
                                <Upload
                                    accept=".jpeg,.jpg"
                                    listType="picture"
                                    multiple={false}
                                    fileList={this.state.fileList}
                                    beforeUpload={this.onBeforePhotoUpload}
                                >
                                    <Button htmlType="button">
                                        <UploadOutlined /> Загрузить
                                    </Button>
                                </Upload>
                            </Form.Item>
                            <Alert
                                className="mb-2"
                                message="Мы нигде не публикуем фотографии и используем их исключительно для модерации."
                                type="success" />
                        </div>
                    } */}

                    <Button
                        className="mb-2"
                        type="primary"
                        shape="round"
                        block
                        size="large"
                        htmlType="submit"
                        disabled={this.state.loading}>
                        Сохранить
                    </Button>

                    <Button shape="round" size="large" block htmlType="button" onClick={this.onCancel}>
                        Закрыть
                    </Button>
                </Form>
            </div>
        )
    }
}

export default AgitationForm;
