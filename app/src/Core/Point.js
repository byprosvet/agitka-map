export const ApprovalStatus = {
  IN_REVIEW: '0',
  APPROVED: '1',
  REJECTED: '2'
};


export function filterPoints(points, {docIds}) {
  const docIdSet = new Set(docIds);

  return docIdSet.size ?
      points.filter(point => docIdSet.has(point.document_id)) :
      points;
}
