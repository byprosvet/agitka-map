import React from 'react';
import { Alert } from 'antd';

const HelpModalContent = () => {
    return (
        <div className="content">
            <h4>Дорогие друзья!</h4>
            <p>
                Помогите нам, пожалуйста, распространить материалы среди Ваших соседей, друзей и знакомых.
                Для этого нужно выполнить несколько шагов:
            </p>
            <ol>
                <li>
                    <p>
                        Скачайте и распечатайте материалы.
                    </p>

                    <Alert
                        className="mb-2"
                        type="success"
                        message="Вы можете использовать двустороннюю печать для экономии."
                        showIcon />
                </li>
                <li>
                    <p>
                        Распространите материалы по почтовым ящикам. {/* {!props.isAdmin &&
                            <span><strong>Сделайте фотографию</strong> распространяемых материалов для подтверждения.</span>} */}
                    </p>
{/* 
                    <Collapse className="mb-2" defaultActiveKey={['1']}>
                        <Collapse.Panel
                            className="photo-panel"
                            header={props.isAdmin ? 'Пример' : 'Пример фотографии'}
                            key="1"
                        >
                            <img
                                className="width100"
                                src={process.env.PUBLIC_URL + '/photo-example.jpg'}
                                alt="Пример фотографии"/>
                        </Collapse.Panel>
                    </Collapse> */}

                    <Alert
                        className="mb-2"
                        type="warning"
                        message={
                            <span>Пожалуйста, <strong>не распространяйте</strong> материалы в местах, не предназначенных для этого.</span>
                        }
                        showIcon />
                </li>
                <li>
                    <p>
                        Отметьте на карте место, где Вы распространили материалы.
                    </p>
                </li>
            </ol>

            <p className="mb-0">Большое спасибо за Вашу помощь!</p>
        </div>
    );
}

export default HelpModalContent;
