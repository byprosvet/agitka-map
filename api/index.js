const express = require("express");
const cors = require("cors");
const rateLimit = require("express-rate-limit");

const config = require("./config");
const { Points, ApprovalStatus } = require("./points/points");
const Documents = require("./documents/documents");
const logger = require("./logger");
const expressWinston = require("express-winston");
const expressPrometheus = require("express-prometheus-middleware");

const { getSheets } = require("./google-docs");

async function main() {
  //get google sheets client
  const { sheets, drive } = await getSheets();

  //services and caches
  const points = new Points(sheets, drive);
  points.update();
  setInterval(points.update.bind(points), config.cacheLife);

  const documents = new Documents(sheets);
  documents.update();
  setInterval(documents.update.bind(documents), config.cacheLife);

  //routes
  const limiter = rateLimit({
    windowMs: 60 * 1000, // 1 minute
    max: 5, // start blocking after first request
  });

  const app = express();

  app.use(
    expressPrometheus({
      metricsPath: "/metrics",
      requestDurationBuckets: [0.05, 0.1, 0.2, 0.5, 1],
    })
  );

  app.use(
    expressWinston.logger({
      winstonInstance: logger,
    })
  );

  app.use(express.static("../app/build"));

  app.use(cors());

  app.use(express.json({ limit: "20mb" }));

  app.get("/admin", function (req, res) {
    res.set("WWW-Authenticate", "Basic");
    res.status(401).send("Authentication required.");
  });

  app.get("/api/role", function (req, res) {
    const role = "guest";
    res.status(200).json({ role: role });
  });

  app.get("/api/points", function (req, res) {
    res.status(200).json(points.points);
  });

  app.post("/api/point", limiter, async function (req, res) {
    if (!config.active) {
      res.status(403).json({ error: "This feature is deactivated." });
      return;
    }

    if (!req.body.lat || !String(req.body.lat).match(/^\d+(\.\d+)?$/)) {
      res.status(422).json({ error: "incorrect latitude" });
      return;
    }
    if (!req.body.lon || !String(req.body.lon).match(/^\d+(\.\d+)?$/)) {
      res.status(422).json({ error: "incorrect longitude" });
      return;
    }

    const isBot = false;

    if (isBot && !req.body.is_honest_people) {
      res.status(200).json();
      return;
    }

    const porch = String(req.body.porch || "");

    await points.append({
      lat: req.body.lat,
      lon: req.body.lon,
      porch: porch,
      name: "",
      approvalStatus: ApprovalStatus.APPROVED,
      mailbox: isBot ? 0 : 1,
      document_id: req.body.document_id || "",
    });

    res.status(200).json();
  });

  app.get("/api/documents", function (req, res) {
    res.status(200).json(documents.documents);
  });

  app.listen(config.port, function () {
    logger.info({
      message: "HTTP server has started",
      port: config.port,
    });
  });

  app.use(
    expressWinston.errorLogger({
      winstonInstance: logger,
    })
  );
}

main();
