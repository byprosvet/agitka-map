export function createAgitationPlacemark(ymaps, coords) {
    const balloonContent = `
        <div class="balloon-content">
            <p class="mb-0">
                <strong>Распространили материалы по этому адресу?</strong>
            </p>
            <p class="mb-1">
                Пожалуйста, добавьте эту точку на карту.
            </p>

            <div>
                <button class='ant-btn ant-btn-sm1 ant-btn-primary ant-btn-round mr-1' data-balloon-action="add">
                    Добавить на карту
                </button>

                <button class='ant-btn ant-btn-sm1 ant-btn-round' data-balloon-action="cancel">
                    Отменить
                </button>
            </div>
        </div>`;

    return new ymaps.Placemark(coords, {
        balloonContent,
        iconCaption: 'Я провел информирование!',
        hintContent: 'Кликнете на точку, что сохранить ее на карте.'
    }, {
        preset: 'islands#circleDotIcon',
        iconColor: '#1890ff',
        draggable: true,
        hideIconOnBalloonOpen: false,
    });
}
