import React, { Component } from 'react';
import { Button, List } from 'antd';
import ActionLink from "./ActionLink";
import SocialShareButtonList from './SocialShareButtonList';
import BRAND from '../brand';

class NavMenu extends Component {
    onVolunter = () => {
        this.props.onHideMenu();
        // 65931997, 'reachGoal', 'volunter');
    }

    render() {
        return (
            <div>
                <List
                    className="mb-2"
                    itemLayout="horizontal"
                    dataSource={BRAND.menu.links}
                    renderItem={item => (
                        <List.Item>
                            <ActionLink link={item.link} target='_blank' handleClick={this.props.onHideMenu}>
                                {item.text}
                            </ActionLink>
                        </List.Item>
                    )}
                >
                </List>

                <div className="social-buttons mb-2">
                    <span>Поделиться в социальных сетях:</span>
                    <SocialShareButtonList />
                </div>
                
                {BRAND.menu.volonteerFormUrl &&
                    <Button
                        className="mb-2"
                        type="primary"
                        shape="round"
                        block
                        size="large"
                        onClick={this.onVolunter}
                    >
                        <a
                            href={BRAND.menu.volonteerFormUrl}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Стать волонтером
                        </a>
                    </Button>
                }
            </div>
        );
    }
}

export default NavMenu;