const { google } = require("googleapis");
const config = require("./config");

async function getGAuth(credentialsPath, tokenPath) {
  const {
    client_secret,
    client_id,
    redirect_uris,
  } = config.gAuth.credential.installed;

  const oAuth2Client = new google.auth.OAuth2(
    client_id,
    client_secret,
    redirect_uris[0]
  );
  oAuth2Client.setCredentials(config.gAuth.token);

  return oAuth2Client;
}

async function getSheets() {
  const gAuth = await getGAuth(config.gAuth.credential, config.gAuth.token);
  const sheets = google.sheets({ version: "v4", auth: gAuth });
  const drive = google.drive({ version: "v3", auth: gAuth });

  return { sheets, drive };
}

module.exports = {
  getGAuth,
  getSheets,
};
