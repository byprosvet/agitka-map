const Documents = require("./documents");
const config = require("../config");
const { getSheets } = require("../google-docs");

let documents;
const createDocuments = async (sheets) => {
  documents = new Documents(sheets);
  await documents.update();
  setInterval(documents.update.bind(documents), config.cacheLife);
};

module.exports = async (req, res) => {
  const { sheets } = await getSheets();
  if (!documents) {
    await createDocuments(sheets);
  }
  res.status(200).json(documents.documents);
};
