const brand = {
  color: "#65A836",
  social: {
    hashTags: ["agitka2020"],
    shareUrl: "https://www.agitka2020.org",
    message: "Поможешь распространить информационные материалы? Это просто!",
  },
  menu: {
    links: [
      {
        link: "https://t.me/by_prosvet",
        text: "Телеграм-канал ByProsvet",
      },
      {
        link: "https://t.me/by_prosvet_bot",
        text: "ByProsvet Bot",
      },
      {
        link: "https://byprosvet.org/",
        text: "byprosvet.org - весь самиздат вместе",
      },
      {
        link: "http://dze.chat/",
        text: "dze.chat - Карысныя чаты Беларусі на мапе",
      },
      {
        link: "https://honest-people.by/",
        text: "*Честные люди",
      },
      {
        link: "https://probono.by/",
        text: "Probono.by - Информационный центр открытых инициатив",
      },
      {
        link: "https://bychange.me/",
        text: "ByChange - Помощь в трудоустройстве",
      },
    ],
  },
};

export default brand;
