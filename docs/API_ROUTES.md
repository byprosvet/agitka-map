### Routes

1. `/auth`

   Роут для авторизации

### API

1. `GET` `/api/role`

    ```json
    ```

    200

    ```json
    {
      "role": "user"
    }
    ```

1. `GET` `/api/points`

    ```json
    ```

    200

    ```json
    [
        {
           "lat": "45453",
           "lon": "43434",
           "porch": "45",
           "approvalStatus": "1",
           "document_id": "12"
        }
    ]
    ```

1. `POST` `api/point`

    ```json
    {
        "lat": 228.282,
        "lon": 42.42,
        "porch": 100,
        "document_id": 12
    }
    ```

    200

    ```json
    ```

1. `GET` `/api/documents`

    ```json
    ```

    200

    ```json
    [
        {
            "id": 2,
            "title": "Листовка номер один",
            "file_link": "https://vk.com",
            "color": "#FF0000"
        },
        {
            "id": 3,
            "title": "Листовка номер два. Очень важная листовка!",
            "file_link": "https://www.example.com",
            "color": "#0000FF"
        }
    ]
    ```
